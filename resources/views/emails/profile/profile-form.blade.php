@component('mail::message')
    # Dear Consultant

    A booking form has been submitted from {{ config('app.name') }}. Please find below details and action accordingly:

<strong>Title:</strong>{{$input['title']}}<br>
<strong>Surname:</strong>{{$input['Surname']}}<br>
<strong>First Names:</strong>{{$input['first_names']}}<br>
<strong>ID Number:</strong>{{$input['id_number']}}<br>
<strong>Gender:</strong>{{$input['gender']}}<br>
<strong>Date Of Birth:</strong>{{$input['date_of_birth']}}<br>
<strong>Nationality:</strong>{{$input['nationality']}}<br>
<strong>Passport Number:</strong>{{$input['passport_number']}}<br>
<strong>Suburb:</strong>{{$input['suburb']}}<br>
<strong>City:</strong>{{$input['city']}}<br>
<strong>Province:</strong>{{$input['province']}}<br>
<strong>Passenger Number:</strong>{{$input['passenger_mobile_num']}}<br>
<strong>Passenger Email:</strong>{{$input['passenger_email']}}<br>
<strong>Special Request:</strong>{{$input['special_requests']}}<br>
<strong>Emergency Contact Full Names:</strong>{{$input['emergency_contact_fullname']}}<br>
<strong>Emergency Contact Number:</strong>{{$input['emergency_contact_number']}}<br>
<strong>Emergency Contact Email:</strong>{{$input['emergency_contact_email']}}<br>

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
