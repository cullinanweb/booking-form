@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row"></div>

        @if(isset(Auth::user()->email))
            <script>window.location="/profile";</script>
        @endif


        <form class="form-horizontal" id="sample_form"   role="form" method="POST" action="{{ route('doLogin') }}">
            {{ csrf_field() }}
        <div class="col-sm-7" id="background">
            <img src="{{url('/images/4036fd1bf88ed153a250f46a05d64ecd.jpg')}}" alt="Image" class="img-responsive">
        </div>
<?php //echo Carbon\Carbon::now() . "\n";
?>
        <div class="col-sm-4 booking" >
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <img src="{{url('/images/download.png')}}" alt="logo" class="img-responsive"><br><br>

                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="form-group" hidden="hidden" id="cons_name">
                <select class="form-control " name="cons_name" id="cons_name" required>
                    @foreach ($users as $user)
                        <option value="{{$user->name}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group"  id="cons_email">
                <select class="form-control " name="cons_email" id="cons_email" required>
                    <option value="">Select Consultant</option>
                @foreach ($users as $user)
                        <option value="{{$user->email}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group{{ $errors->has('cons_ref') ? ' has-error' : '' }}">

                    <input id="cons_ref" type="text" class="form-control" name="cons_ref" placeholder="Reference" required>

                    @if ($errors->has('cons_ref'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('cons_ref') }}</strong>
                                    </span>
                    @endif
            </div>
                <div class="wrapper">
                <button class="btn btn-info"  type="submit" >Create Profile Link</button>
                    <br>
                    <br>
                </div>
            <?php //dd($user);?>





                <div class="col-sm-offset-3"><div class="wrapper" id="last_inserted_id"></div></div>
            <div class="col-sm-1 ">

            </div>

        </div>
        </form>
    </div>
    <script>

        $(document).on('submit', '#sample_form', function(event) {

            event.preventDefault();
            var form_contents = $(this).serialize();

            $.post("{{ route('doLogin') }}", form_contents, function (data) {

                $('#sample_form')[0].reset();
                $('#last_inserted_id').html('  <?php echo $_SERVER['HTTP_HOST']; ?>/profile/' + data.id + '/edit');
            }, 'json');
           // $_SERVER['REQUEST_URI'] .'://' . $_SERVER['HTTP_HOST']; ?>/profile/' + data.id + '/edit'

        });
    </script>



@endsection
