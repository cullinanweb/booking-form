@extends('layouts.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-8">
                        <h3 class="box-title">List Of Consultants</h3>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-primary" href="{{ route('consultant.create') }}">Create new Consultant</a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <td>Consultant Name</td>
                    <td>Email</td>
                    <td>Reference</td>
                    <td>Code</td>
                    <td>Actions</td>
                </tr>
                </thead>
                <tbody>

                @foreach($users as $key => $value)

                    <tr>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->email }}</td>
                        <td> {{$value->ref }}</td>
                        <td> {{$value->code }}</td>
                        <!-- we will also add show, edit, and delete buttons -->
                        <td>

                            <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                            <!-- we will add this later since its a little more complicated than the other two buttons -->

                            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <a href="{{ route('consultant.destroy', ['id' => $value->id]) }}" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                                Remove Consultant
                            </a>
                            <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </section>
    <!-- /.content -->
@endsection