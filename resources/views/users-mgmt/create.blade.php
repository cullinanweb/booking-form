@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add New Consultant</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ route('consultant.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}" style="">
                                <label for="name" class="col-md-4 control-label">Full Names<a
                                            class="text-red">*</a></label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address<a
                                            class="text-red">*</a></label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('ref') ? ' has-error' : '' }}">
                                <label for="ref" class="col-md-4 control-label">Reference Number<a
                                            class="text-red">*</a></label>

                                <div class="col-md-6">
                                    <input id="ref" type="ref" class="form-control" name="ref" required>

                                    @if ($errors->has('ref'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ref') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">


                                <div class="col-md-6">
                                    <input id="code" type="hidden" class="form-control" name="code" required>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create Consultant
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
