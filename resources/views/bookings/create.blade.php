@extends('layouts.header')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Client Info</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <!-- Form -->
                                    <form action="{{route('profile.store')}}" method="POST" role="form"
                                          class="form-horizontal">
                                        {{csrf_field()}}
                                        <fieldset class="col-md-12">
                                            <div class="form-group row">
                                                <p>Dear Client
                                                    This booking form is the sole contract between Thompsons and
                                                    yourself and no express terms, undertakings, or warranties not
                                                    contained herein will be valid. Thompsons undertakes to provide the
                                                    services that are detailed under your booking number and you hereby
                                                    agree to our Standard Terms and Conditions available on request and
                                                    on our website at (www.thompsons.co.za). By signing this booking
                                                    form you are deemed to have read, understood and accepted the
                                                    Thompsons Terms and Conditions and you agree to comply with them.
                                                    Your signature also means that you have the authority and
                                                    contractual capacity to act on behalf of and bind the other people
                                                    whose names appear on this booking form. If you do not have this
                                                    authority they need to complete their own booking form. We need to
                                                    have the details of your next of kin or someone that you would like
                                                    us to contact in case of emergency or major change in your travel
                                                    itinerary. Please fill in below. We would like to draw your specific
                                                    attention to the fact that you are responsible for your own
                                                    passports, visa’s vaccinations and inoculations. Please return this
                                                    form to your agent – <b>Wendy Schulze</b></p>
                                                <p><b>Passenger names as they appear in your passport (ID document for
                                                        local travel)</b></p>

                                                <p><b>Please note: clear legible passport copies required immediately
                                                        upon booking</b>
                                                </p><br>
                                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                                    <label for="title" class="col-sm-3 col-form-label">Title<a
                                                                class="text-red">*</a></label>
                                                    <select name="title" class="col-md-6 form-control">
                                                        <option value="">Select Title</option>
                                                        <option value="Mrs"@if (old('title')=="Mrs"){{'selected'}} @endif>
                                                            Mrs
                                                        </option>
                                                        <option value="Mr"@if (old('title')=="Mr"){{'selected'}} @endif>
                                                            Mr
                                                        </option>
                                                        <option value="Miss"@if (old('title')=="Miss"){{'selected'}} @endif>
                                                            Miss
                                                        </option>
                                                        <option value="Dr"@if (old('title')=="Dr"){{'selected'}} @endif>
                                                            Dr
                                                        </option>
                                                        <option value="Prof"@if (old('title')=="Prof"){{'selected'}} @endif>
                                                            Prof
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('first_names') ? ' has-error' : '' }} row">
                                                <label for="profile_name"
                                                       class="col-sm-3 col-form-label">Surname</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="surname"
                                                           value="{{ old('surname') }}" name="surname"
                                                           placeholder="Surname">
                                                </div>
                                                @if ($errors->has('surname'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('first_names') ? ' has-error' : '' }} row">
                                                <label for="profile_description" class="col-sm-3 col-form-label">First
                                                    Names</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="first_names"
                                                           value="{{ old('first_names') }}"
                                                           name="first_names" placeholder="Full Names">
                                                </div>
                                                @if ($errors->has('first_names'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_names') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }} row">
                                                <label for="profile_description" class="col-sm-3 col-form-label">ID
                                                    Number</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="id_number"
                                                           value="{{ old('id_number') }}"
                                                           name="id_number" placeholder="e.g 9201090737088"
                                                           onkeypress="return isNumber(event)">
                                                </div>
                                                @if ($errors->has('id_number'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_number') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group row">
                                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                                    <label for="title" class="col-sm-3 col-form-label">Gender<a
                                                                class="text-red">*</a></label>
                                                    <select name="gender" class="form-control">
                                                        <option value="">Gender</option>
                                                        <option value="Female"@if (old('gender')=="Female"){{'selected'}} @endif>
                                                            Female
                                                        </option>
                                                        <option value="Male"@if (old('gender')=="Male"){{'selected'}} @endif>
                                                            Male
                                                        </option>
                                                    </select>
                                                </div>
                                                @if ($errors->has('gender'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group row{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label">Date of Birth<a
                                                            class="text-red">*</a></label>
                                                <div class="col-md-6">
                                                    <div class="input-group date" data-provide="datepicker"
                                                         data-date-format="yyyy-mm-dd">
                                                         <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                                        <input type="text" value="{{ old('date_of_birth') }}"
                                                               name="date_of_birth"
                                                               class="form-control pull-right" id="date_of_birth">
                                                    </div>

                                                    @if ($errors->has('date_of_birth'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row{{ $errors->has('nationality') ? ' has-error' : '' }}">

                                                <label for="nationality"
                                                       class="col-md-4 control-label">Nationality</label>

                                                <div class="col-md-6">
                                                    <input id="nationality" type="text" class="form-control"
                                                           name="nationality"
                                                           value="{{ old('nationality') }}">

                                                    @if ($errors->has('nationality'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('nationality') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row{{ $errors->has('passport_number') ? ' has-error' : '' }}">

                                                <label for="passport_number" class="col-md-4 control-label">Passport
                                                    Number</label>

                                                <div class="col-md-6">
                                                    <input id="passport_number" type="text" class="form-control"
                                                           name="passport_number"
                                                           value="{{ old('passport_number') }}"
                                                           onkeypress="return isNumber(event)">

                                                    @if ($errors->has('passport_number'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('passport_number') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row{{ $errors->has('suburb') ? ' has-error' : '' }}">
                                                <label class="col-sm-3 col-form-label">Suburb<a
                                                            class="text-red">*</a></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="suburb"
                                                           value="{{ old('suburb') }}" name="suburb"
                                                           placeholder="suburb">
                                                </div>
                                            </div>
                                            <div class="form-group row{{ $errors->has('city') ? ' has-error' : '' }}">
                                                <label class="col-sm-3 col-form-label">City<a
                                                            class="text-red">*</a></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="city"
                                                           value="{{ old('city') }}" name="city"
                                                           placeholder="city">
                                                </div>
                                            </div>
                                            <div class="form-group row{{ $errors->has('province') ? ' has-error' : '' }}">
                                                <label class="col-sm-3 col-form-label">Province<a class="text-red">*</a></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="province"
                                                           value="{{ old('province') }}" name="province"
                                                           placeholder="province">
                                                </div>
                                                @if ($errors->has('province'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group row{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                                <label class="col-sm-3 col-form-label">Postal Code<a
                                                            class="text-red">*</a></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="postal_code"
                                                           value="{{ old('postal_code') }}" name="postal_code"
                                                           placeholder="postal_code"
                                                           onkeypress="return isNumber(event)">
                                                </div>
                                                @if ($errors->has('postal_code'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('postal_code') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('passenger_number') ? ' has-error' : '' }} row">
                                                <label for="passenger_number" class="col-sm-3 col-form-label">Passenger
                                                    Number</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="passenger_number"
                                                           value="{{ old('passenger_number') }}"
                                                           name="passenger_number" placeholder="passenger_number">
                                                </div>
                                                @if ($errors->has('passenger_number'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('passenger_number') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('passenger_email') ? ' has-error' : '' }} row">
                                                <label for="passenger_email" class="col-sm-3 col-form-label">Passenger
                                                    Email</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="passenger_email"
                                                           value="{{ old('passenger_email') }}"
                                                           name="passenger_email" placeholder="passenger_email">
                                                </div>
                                                @if ($errors->has('passenger_email'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('passenger_email') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group row">
                                                <label for="special_requests" class="col-sm-3 col-form-label">Special
                                                    Requests</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="special_requests"
                                                           value="{{ old('special_requests') }}"
                                                           name="special_requests" placeholder="special_requests">
                                                </div>

                                            </div>
                                            <div class="form-group row{{ $errors->has('emergency_contact_fullname') ? ' has-error' : '' }}">
                                                <label for="emergency_contact_fullname" class="col-sm-3 col-form-label">Emergency
                                                    Contact
                                                    Full Names<a class="text-red">*</a></label>

                                                <div class="col-md-6">
                                                    <input id="emergency_contact_fullname" type="text"
                                                           class="form-control"
                                                           name="emergency_contact_fullname"
                                                           value="{{ old('emergency_contact_fullname') }}"
                                                    >

                                                    @if ($errors->has('emergency_contact_fullname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('emergency_contact_fullname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row{{ $errors->has('emergency_contact_number') ? ' has-error' : '' }}">
                                                <label for="emergency_contact_number" class="col-sm-3 col-form-label">Emergency
                                                    Contact
                                                    Number<a class="text-red">*</a></label>

                                                <div class="col-md-6">
                                                    <input id="emergency_contact_number" type="text"
                                                           class="form-control"
                                                           name="emergency_contact_number"
                                                           value="{{ old('emergency_contact_number') }}"
                                                           onkeypress="return isNumber(event)"
                                                    >

                                                    @if ($errors->has('emergency_contact_number'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('emergency_contact_number') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row{{ $errors->has('emergency_contact_relation') ? ' has-error' : '' }}">
                                                <label for="emergency_contact_relation" class="col-sm-3 col-form-label">Emergency
                                                    Contact
                                                    Email<a class="text-red">*</a></label>

                                                <div class="col-md-8">
                                                    <input id="emergency_contact_relation" type="text"
                                                           class="form-control"
                                                           name="emergency_contact_relation"
                                                           value="{{ old('emergency_contact_relation') }}">

                                                    @if ($errors->has('emergency_contact_relation'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('emergency_contact_relation') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" required>
                                                <label for="terms"> I have read, understood and accepted the Thompsons<br>
                                                    Terms and Conditions and am duly authorised to sign on behalf of the
                                                    people listed above.

                                                </label><br>
                                            </div>


                                            <div class="row">
                                                <div class="col-sm-12" id="call">
                                                    <p class="text-center">Thompsons For Travel – Please email proof of
                                                        payment to <a href="mailto:travel@thompsons.co.za">travel@thompsons.co.za</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <h5><b>STANDARD BANK</b></h5>
                                                    <p><b>Swift Code:</b> SBZAZAJJ
                                                        <b>Name:</b> THOMPSONS TRAVEL<br>
                                                        <b>Account Number:</b> 022284192<br>
                                                        <b>Branch Code:</b> 006605</p>

                                                </div>
                                                <div class="col-sm-3">
                                                    <h5><b>ABSA</b></h5>
                                                    <p>
                                                        <b>Swift Code:</b> ABSAZAJJ
                                                        <b>Name:</b> THOMPSONS TRAVEL<br>
                                                        <b>Account Number:</b> 4077212842<br>
                                                        <b>Branch Code: 632005</b></p>

                                                </div>
                                                <div class="col-sm-3">
                                                    <h5><b>FNB</b></h5>
                                                    <p><b>Swift Code:</b> FIRNZAJJ
                                                        <b>Name:</b> THOMPSONS TRAVEL<br>
                                                        <b>Account Number:</b> 62297845194<br>
                                                        <b>Branch:</b> 250655</p>

                                                </div>
                                                <div class="col-sm-3">
                                                    <h5><b>NEDBANK</b></h5>
                                                    <p><b>Swift Code:</b> NEDSZAJJ
                                                        <b>Name:</b> THOMPSONS TRAVEL<br>
                                                        <b>Account Number:</b> 1972066935<br>
                                                        <b>Branch:</b> 197205</p>

                                                </div>
                                            </div>
                                        </fieldset>


                                        <div class="offset-sm-2 col-sm-3">
                                        </div>
                                        <div class="offset-sm-2 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Update Profile</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
