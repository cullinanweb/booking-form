@extends('layouts.app')

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-8">
                        <h3 class="box-title">List Of Profiles</h3>
                    </div>
                    <div class="col-sm-4" style="display: none">
                        <a class="btn btn-primary"  href="{{ url('/downloadPDF/download ') }}">Download PDF</a>
                        <a class="btn btn-primary" href="{{ route('profile.create') }}">Create new profile link</a>
                        <a class="btn btn-primary" onclick="window.print()">Print this page</a>

                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <td width="5%" rowspan="1" colspan="1">Profile Link</td>
                    <td width="8%" rowspan="1" colspan="1">Consultant Ref</td>
                    <td width="15%" rowspan="1" colspan="1">Triggers</td>
                    <td width="10%" rowspan="1" colspan="1">Updated Time</td>
                    <td width="20%" rowspan="1" colspan="1">Edit Profile</td>
                    <td width="20%" rowspan="1" colspan="1">PDF</td>

                </tr>
                </thead>
                <tbody>

                @foreach($profiles as $key => $value)

                    <tr>
                        <td>{{ route('profile.edit', ['id' => $value->id]) }}</td>

                        <td>{{ $value->cons_ref }}</td>
                        <!-- we will also add show, edit, and delete buttons -->
                        <td>

                            <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                            <!-- we will add this later since its a little more complicated than the other two buttons -->

                            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @php $currentTime = new DateTime();
                            $startTime = ($value->updated_at);
                            $diff= $startTime->diff($currentTime);

                            $hours = $diff->h;
                            $hours=$hours +($diff->days*24);
//dd($diff);

                            if ($hours > 48 & empty($value->passenger_email)){
                             echo "Time elapsed is <b>$hours</b> hours <br />";
                            echo "Over <b>48 </b> hours <br>";
                            }
                            elseif ($hours>24 & empty($value->passenger_email)){
                            echo "Over <b>24</b> Hours <br>";
                            }
                            elseif (!empty($value->passenger_email)){
                            echo '<label style="color:green;" class="col-md-4  text-green control-label">Updated</label>';
                            }
                            elseif ($hours<24 & empty($value->passenger_email)){
                            echo " <label style='color:#e68e0a;'>Time elapsed is <b>$hours</b> hours <br />";
                            }





                            @endphp
                        </td>
                        <td> {{$value->updated_at}}</td>

                        <td>
                            <a href="{{ route('profile.edit', ['id' => $value->id]) }}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                                Edit Profile
                            </a>

                        <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->

                        </td>
                        <td><a href="{{action('ProfileController@downloadPDF', $value->id)}}"class="btn-success  col-sm-3 col-xs-5 btn-margin">Download</a></td>

                    </tr>
        @endforeach

                </tbody>
            </table>
        </div>
    </section>
    <!-- /.content -->
@endsection