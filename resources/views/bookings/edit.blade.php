@extends('layouts.header')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (!empty($profile->passenger_email))
                    <td><a href="{{action('ProfileController@downloadPDF', $profile->id)}}"class="btn-success  col-sm-12 col-xs-5 btn-margin text-center"><b>Download your profile</b></a></td>

                <div class="panel panel-default">

                    <div class="panel-body">


                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Client Info</div>

                    <div class="panel-body">

                        @if ($message = session('success'))
                            <div class="alert alert-success alert-block">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <!-- Form -->
                                    <form action="{{route('profile.update', ['id' => $profile->id])}}" method="POST"
                                          role="form" class="form-horizontal" enctype="multipart/form-data" onsubmit="return checkForm(this);">
                                        {{ method_field('PATCH') }}
                                        <input type="hidden" name="_token"
                                               value="{{ csrf_token() }}">

                                        <fieldset class="col-md-12">
                                            <div class="form-group row" id="myDiv">
                                                <p>Dear Client<br>
                                                    This booking form is the sole contract between Thompsons and
                                                    yourself and no express terms, undertakings, or warranties not
                                                    contained herein will be valid. Thompsons undertakes to provide the
                                                    services that are detailed under your booking number and you hereby
                                                    agree to our Standard Terms and Conditions available on request and
                                                    on our website at (www.thompsons.co.za). By signing this booking
                                                    form you are deemed to have read, understood and accepted the
                                                    Thompsons Terms and Conditions and you agree to comply with them.
                                                    Your signature also means that you have the authority and
                                                    contractual capacity to act on behalf of and bind the other people
                                                    whose names appear on this booking form. If you do not have this
                                                    authority they need to complete their own booking form. We need to
                                                    have the details of your next of kin or someone that you would like
                                                    us to contact in case of emergency or major change in your travel
                                                    itinerary. Please fill in below. We would like to draw your specific
                                                    attention to the fact that you are responsible for your own
                                                    passports, visa’s vaccinations and inoculations. Please return this
                                                    form to your agent – <b>{{$profile->cons_name}}</b><br><b>Email: {{$profile->cons_email}}<br>
                                                        REF: {{$profile->cons_ref}}</b></p>
                                                <p><b>Passenger names as they appear in your passport (ID document for
                                                        local travel)</b></p>

                                                <p><b>Please note: clear legible passport copies required immediately
                                                        upon booking</b>
                                                </p><br>
                                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                                    <label for="title" class="col-sm-3 col-form-label">Title<a
                                                                class="text-red">*</a></label>
                                                    <div class="col-sm-9">
                                                        <select name="title" class="col-md-6 form-control">
                                                            <option value="{{$profile->title}}">{{$profile->title}}</option>
                                                            <option value="Mrs">Mrs</option>
                                                            <option value="Mr">Mr</option>
                                                            <option value="Miss">Miss</option>
                                                            <option value="Dr">Dr</option>
                                                            <option value="Prof">Prof</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('Surname') ? ' has-error' : '' }} row">
                                                    <label for="Surname"
                                                           class="col-sm-3 col-form-label">Surname</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="Surname"
                                                               value="{{ $profile->Surname }}" name="Surname"
                                                               placeholder="Surname">
                                                    </div>
                                                    @if ($errors->has('surname'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                                    @endif
                                                </div>

                                                <div class="form-group{{ $errors->has('first_names') ? ' has-error' : '' }} row">
                                                    <label for="first_names" class="col-sm-3 col-form-label">First
                                                        Names</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="first_names"
                                                               value="{{ $profile->first_names }}"
                                                               name="first_names" placeholder="Full Names">
                                                    </div>
                                                    @if ($errors->has('first_names'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_names') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group{{ $errors->has('id_number') ? ' has-error' : '' }} row">
                                                    <label for="id_number" class="col-sm-3 col-form-label">ID
                                                        Number</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="id_number"
                                                               value="{{ $profile->id_number }}"
                                                               name="id_number" placeholder="e.g 9201090737088"
                                                               onkeypress="return isNumber(event)">
                                                    </div>
                                                    @if ($errors->has('id_number'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('id_number') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                                    <label for="gender" class="col-sm-3 col-form-label">Gender<a
                                                                class="text-red">*</a></label>
                                                    <div class="col-sm-9">
                                                        <select name="gender" class="form-control">
                                                            <option value="{{$profile->gender}}">{{$profile->gender}}</option>
                                                            <option value="Female">
                                                                Female
                                                            </option>
                                                            <option value="Male">
                                                                Male
                                                            </option>
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('gender'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group row{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                                                    <label for="date_of_birth" class="col-sm-3 col-form-label">Date of
                                                        Birth<a
                                                                class="text-red">*</a></label>
                                                    <div class="col-md-6">

                                                        <div class="input-group date" id="datepicker" data-provide="datepicker"
                                                             data-date-format="yyyy-mm-dd">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="date" value="{{ $profile->date_of_birth }}"
                                                                   name="date_of_birth"
                                                                   class="form-control pull-right" id="date_of_birth">
                                                        </div>

                                                        @if ($errors->has('date_of_birth'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row{{ $errors->has('nationality') ? ' has-error' : '' }}">

                                                    <label for="nationality"
                                                           class="col-sm-3 col-form-label">Nationality</label>

                                                    <div class="col-md-6">
                                                        <input id="nationality" type="text" class="form-control"
                                                               name="nationality"
                                                               value="{{ $profile->nationality }}">

                                                        @if ($errors->has('nationality'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('nationality') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">

                                                    <label for="passport_number" class="col-sm-3 col-form-label">Passport
                                                        Number</label>

                                                    <div class="col-md-6">
                                                        <input id="passport_number" type="text" class="form-control"
                                                               name="passport_number"
                                                               value="{{ $profile->passport_number }}"
                                                               onkeypress="return isNumber(event)">


                                                    </div>
                                                </div>
                                                <div class="form-group row{{ $errors->has('suburb') ? ' has-error' : '' }}">
                                                    <label class="col-sm-3 col-form-label">Suburb<a
                                                                class="text-red">*</a></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="suburb"
                                                               value="{{ $profile->suburb }}" name="suburb"
                                                               placeholder="suburb">
                                                    </div>
                                                </div>
                                                <div class="form-group row{{ $errors->has('city') ? ' has-error' : '' }}">
                                                    <label class="col-sm-3 col-form-label">City<a
                                                                class="text-red">*</a></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="city"
                                                               value="{{ $profile->city }}" name="city"
                                                               placeholder="city">
                                                    </div>
                                                </div>
                                                <div class="form-group row{{ $errors->has('province') ? ' has-error' : '' }}">
                                                    <label class="col-sm-3 col-form-label">Province<a
                                                                class="text-red">*</a></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="province"
                                                               value="{{ $profile->province }}" name="province"
                                                               placeholder="province">
                                                    </div>
                                                    @if ($errors->has('province'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group row{{ $errors->has('street_address') ? ' has-error' : '' }}">
                                                    <label class="col-sm-3 col-form-label">Postal Code<a
                                                                class="text-red">*</a></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="street_address"
                                                               value="{{ $profile->street_address }}" name="street_address"
                                                               placeholder="00000"
                                                               onkeypress="return isNumber(event)">
                                                    </div>
                                                    @if ($errors->has('street_address'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('street_address') }}</strong>
                                    </span>
                                                    @endif
                                                </div>

                                                <div class="form-group{{ $errors->has('passenger_mobile_num') ? ' has-error' : '' }} row">
                                                    <label for="passenger_mobile_num" class="col-sm-3 col-form-label">Passenger
                                                        Number</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="passenger_number"
                                                               value="{{ $profile->passenger_mobile_num }}"
                                                               name="passenger_mobile_num"
                                                               placeholder="passenger_mobile_num">
                                                    </div>
                                                    @if ($errors->has('passenger_mobile_num'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('passenger_mobile_num') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group{{ $errors->has('passenger_email') ? ' has-error' : '' }} row">
                                                    <label for="passenger_email" class="col-sm-3 col-form-label">Passenger
                                                        Email</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="passenger_email"
                                                               value="{{ $profile->passenger_email }}"
                                                               name="passenger_email" placeholder="passenger_email">
                                                    </div>
                                                    @if ($errors->has('passenger_email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('passenger_email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group row">
                                                    <label for="special_requests" class="col-sm-3 col-form-label">Special
                                                        Requests</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="special_requests"
                                                               value="{{$profile->special_requests }}"
                                                               name="special_requests" placeholder="special_requests">
                                                    </div>

                                                </div>
                                                <div class="form-group row{{ $errors->has('emergency_contact_fullname') ? ' has-error' : '' }}">
                                                    <label for="emergency_contact_fullname"
                                                           class="col-sm-3 col-form-label">Emergency
                                                        Contact
                                                        Full Names<a class="text-red">*</a></label>

                                                    <div class="col-md-6">
                                                        <input id="emergency_contact_fullname" type="text"
                                                               class="form-control"
                                                               name="emergency_contact_fullname"
                                                               value="{{ $profile->emergency_contact_fullname }}"
                                                        >

                                                        @if ($errors->has('emergency_contact_fullname'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('emergency_contact_fullname') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row{{ $errors->has('emergency_contact_number') ? ' has-error' : '' }}">
                                                    <label for="emergency_contact_number"
                                                           class="col-sm-3 col-form-label">Emergency
                                                        Contact
                                                        Number<a class="text-red">*</a></label>

                                                    <div class="col-md-6">
                                                        <input id="emergency_contact_number" type="text"
                                                               class="form-control"
                                                               name="emergency_contact_number"
                                                               value="{{ $profile->emergency_contact_number }}"
                                                               onkeypress="return isNumber(event)"
                                                        >

                                                        @if ($errors->has('emergency_contact_number'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('emergency_contact_number') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row{{ $errors->has('emergency_contact_email') ? ' has-error' : '' }}">
                                                    <label for="cons_email"
                                                           class="col-sm-3 col-form-label">Emergency
                                                        Contact
                                                        Email<a class="text-red">*</a></label>

                                                    <div class="col-md-8">
                                                        <input id="emergency_contact_email" type="text"
                                                               class="form-control"
                                                               name="emergency_contact_email"
                                                               value="{{ $profile->emergency_contact_email }}">

                                                        @if ($errors->has('emergency_contact_email'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('emergency_contact_email') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <input type="hidden" id="checked" name="checked" value="no" >
                                                    <input type="checkbox" id="checked" class="larger" name="checked" value="yes" style="width: 50px; height: 50px;margin-left: 15px" required>
                                                    <label for="terms"> I have read, understood and accepted the
                                                        Thompsons<br>
                                                        Terms and Conditions and am duly authorised to sign on behalf of
                                                        the
                                                        people listed above.

                                                    </label><br>
                                                </div>


                                                <div class="row">
                                                    <div class="col-sm-12" id="call">
                                                        <p class="text-center">Thompsons For Travel – Please email proof
                                                            of
                                                            payment to <a href="mailto:travel@thompsons.co.za">travel@thompsons.co.za</a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <h5><b>STANDARD BANK</b></h5>
                                                        <p><b>Swift Code:</b> SBZAZAJJ
                                                            <b>Name:</b> THOMPSONS TRAVEL<br>
                                                            <b>Account Number:</b> 022284192<br>
                                                            <b>Branch Code:</b> 006605</p>

                                                    </div>
                                                    <div class="col-sm-3">
                                                        <h5><b>ABSA</b></h5>
                                                        <p>
                                                            <b>Swift Code:</b> ABSAZAJJ
                                                            <b>Name:</b> THOMPSONS TRAVEL<br>
                                                            <b>Account Number:</b> 4077212842<br>
                                                            <b>Branch Code: 632005</b></p>

                                                    </div>
                                                    <div class="col-sm-3">
                                                        <h5><b>FNB</b></h5>
                                                        <p><b>Swift Code:</b> FIRNZAJJ
                                                            <b>Name:</b> THOMPSONS TRAVEL<br>
                                                            <b>Account Number:</b> 62297845194<br>
                                                            <b>Branch:</b> 250655</p>

                                                    </div>
                                                    <div class="col-sm-3">
                                                        <h5><b>NEDBANK</b></h5>
                                                        <p><b>Swift Code:</b> NEDSZAJJ
                                                            <b>Name:</b> THOMPSONS TRAVEL<br>
                                                            <b>Account Number:</b> 1972066935<br>
                                                            <b>Branch:</b> 197205</p>

                                                    </div>
                                                </div>


                                                @if(empty($profile->passenger_email)){


                                        <div class="offset-sm-2 col-sm-3">
                                        </div>
                                        <div class="offset-sm-2 col-sm-9">
                                            <button onclick="myFunction()" type="submit" name="myButton" id="hide" class="btn btn-primary">Save Profile</button>
                                        </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    }
@endif
    <script>
        function myFunction() {
            var x = document.getElementById("myDiv");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>

    <script>
        function checkForm(form)
        {
            //
            // validate form fields
            //

            form.myButton.disabled = true;
            return true;
        }


    </script>
    <script type="text/javascript">
        $(function () {
            $(document).ready(function () {
                $('#datepicker').datepicker();
                autoclose:true;

            })
        });
    </script>

@endsection
