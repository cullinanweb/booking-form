<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //

    public function users(){
        return $this->belongsTo('App\User');
    }

    protected $guarded = [];

    protected $table = 'crud';

    protected $fillable = ['title', 'first_names', 'users_id', 'surname', 'first_names', 'gender', 'date_of_birth', 'nationality',
        'passenger_email', 'street_address', 'suburb', 'city', 'province', 'passenger_mobile_num', 'passport_number', 'emergency_contact_number',
        'emergency_contact_relation', 'emergency_relationship', 'id_number', 'crud_url_unique', 'special_requests'];



}
