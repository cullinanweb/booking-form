<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Profile;
use App\User;

Route::get('/', function () {
    $users = \App\Consultant::get();
    $profiles = Profile::all();
//dd($users);

    // dd($users);
    return view('users-mgmt/index',['users'=>$users]);
});



Route::get('/downloadPDF/download','ProfileController@downloadPDF');
Route::get('/downloadPDF/{id}','ProfileController@downloadPDF');

//Route::put('/bookings', 'API\UserController@putUpdateUser');
Route::resource('profile','ProfileController');
Route::resource('consultant','ConsultantController');
Route::resource('consultants','ConsultantController@index');
Route::resource('/consultant/show','ConsultantController@store');
//Route::resource('/consultant','UserController@store');
Route::post('/consultantsd','ConsultantController@doLogin')->name('doLogin');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );




Route::auth();

Route::get('/home', 'HomeController@index');

