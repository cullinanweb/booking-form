<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Consultant;
use App\Profile;


class ConsultantController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/consultant';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $db_ext = DB::connection('engage');
        $enquiries = $db_ext->table('enquiries')->get();
        print_r($enquiries);
        echo "zanele";
        dd($enquiries);

        $users = Consultant::get();
        $profiles = Profile::get();

        Profile::create([]);
//dd($users);


        // dd($users);
        return view('users-mgmt/index', ['users' => $users, 'countries'=>$enquiries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doLogin(Request $request)
    {
        //$users = User::all();

        //$profiles = Profile::all();

        if ($request->ajax()) {
            $data = array(
                'cons_ref' => $request->cons_ref,
                'cons_email' => $request->cons_email,
                'cons_name' => $request->cons_name,
            );

            //Method 1
            /*$last_id = DB::table('last_ids')
               ->insertGetId($data);*/

            //Method 2
            /*DB::table('last_ids')->insert($data);
            $last_id = DB::getPDO()->lastInsertId();*/

            //Method 3
            /*$result = LastId::create($data);*/


            $last_id = DB::table('crud')
                ->insertGetId($data);
            //Method 4
            //dd($last_id);


            //return view('bookings/create', ['id' => $last_id]);

            return response()->json(['id' => $last_id]);
        }


        //return redirect()->intended('profile', ['profiles'=>$profiles]);
    }


    public function create()

    {


        return view('users-mgmt/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // exit('zanele');
        set_time_limit(0);
        $this->validateInput($request);
        Consultant::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'ref' => $request['ref'],
            //'code' => $request['code'],
        ]);

        return redirect()->intended('/consultant');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $users = Consultant::all();
        $profiles = Profile::all();

        Profile::create([]);
//dd($users);

        // dd($users);
        return view('users-mgmt/show', ['users' => $users,]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Consultant::find($id);
        // Redirect to user list if updating user wasn't existed
        if ($user == null || count($user) == 0) {
            return redirect()->intended('/');
        }

        return view('users-mgmt/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Consultant::findOrFail($id);
        $constraints = [
            'name' => 'required|max:60',
            //'email' => 'required|max:60',
            'ref' => 'required|max:10',
            'code' => 'required|max:10',

        ];
        $input = [
            'name' => $request['name'],
            'ref' => $request['ref'],
            'code' => $request['code'],

        ];

        $this->validate($request, $constraints);
        Consultant::where('id', $id)
            ->update($input);

        return redirect()->intended('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Consultant::where('id', $id)->delete();
        return redirect()->intended('/consultant');
    }

    /**
     * Search user from database base on some specific constraints
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $constraints = [
            'name' => $request['name'],
            'ref' => $request['ref'],
            'email' => $request['email'],
        ];

        $users = $this->doSearchingQuery($constraints);
        return view('bookings/index');
    }

    private function doSearchingQuery($constraints)
    {
        $query = Consultant::query();
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where($fields[$index], 'like', '%' . $constraint . '%');
            }

            $index++;
        }
        return $query->paginate(5);
    }

    private function validateInput($request)
    {
        $this->validate($request, [
            //'username' => 'required|max:60',
            'email' => 'required|email|max:255|unique:users',
            'name' => 'required',
            'ref' => 'required',
            //'code' => 'required',
        ]);
    }
}
