<?php

namespace App\Http\Controllers;

use App\Mail\ProfileFormMail;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use \PDF;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::all();
        $profiles = Profile::all();
        return View('bookings/index', ['profiles' => $profiles, 'users'=>$users]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $users = User::all();
        $profiles = Profile::all();

        return view('bookings/create', ['profiles' => $profiles, 'users' => $users]);
    }


    public function downloadPDF($id) {
        set_time_limit(0);

        $profile = Profile::find($id);
        $pdf = PDF::loadView('pfd', compact('profile'));
        //dd($pdf);

        return $pdf->download('profile.pdf', ['profile'=>$profile]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validateInput($request);
        Profile::create([
            'title' => $request['title'],
            'first_names' => $request['first_names'],
            'Surname' => $request['Surname'],
            //'users_id' => $request['users_id'],
            'gender' => $request['gender'],
            'date_of_birth' => $request['date_of_birth'],
            'nationality' => $request['nationality'],
            'passenger_email' => $request['passenger_email'],
            //'street_address' => $request['street_address'],
            'suburb' => $request['suburb'],
            'city' => $request['city'],
            'province' => $request['province'],
            //'passenger_mobile_num' => $request['passenger_mobile_num'],
            'passport_number' => $request['passport_number'],
            'emergency_contact_number' => $request['emergency_contact_number'],
            //'emergency_contact_relation' => $request['emergency_contact_relation'],
           // 'emergency_relationship' => $request['emergency_relationship'],
            'id_number' => $request['id_number'],
           // 'crud_url_unique' => $request['crud_url_unique'],
            'special_requests' => $request['special_requests'],
        ]);


        return redirect()->intended('profile');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect()->intended('profile');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id);
        // Redirect to user list if updating user wasn't existed

        //$profile = Profile::all();

        return view('bookings/edit',['profile'=>$profile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $profile = Profile::findOrFail($id);
       // $this->validateInput($request);
        //dd($this->validateInput($request));
        // Upload image
        $input = [
            'title' => $request['title'],
            'first_names' => $request['first_names'],
            'Surname' => $request['Surname'],
            'checked' => $request['checked'],
            'gender' => $request['gender'],
            'date_of_birth' => $request['date_of_birth'],
            'nationality' => $request['nationality'],
            'passenger_email' => $request['passenger_email'],
            'street_address' => $request['street_address'],
            'suburb' => $request['suburb'],
            'city' => $request['city'],
            'province' => $request['province'],
            'passenger_mobile_num' => $request['passenger_mobile_num'],
            'passport_number' => $request['passport_number'],
            'emergency_contact_number' => $request['emergency_contact_number'],
            'emergency_contact_fullname' => $request['emergency_contact_fullname'],
            'emergency_contact_email' => $request['emergency_contact_email'],
             //'cons_email' => $request['cons_email'],
            'id_number' => $request['id_number'],
           //'cons_name' => $request['cons_name'],
            'special_requests' => $request['special_requests'],
        ];
        Profile::where('id', $id)
            ->update($input);
        //dd($input);



        $pdf = PDF::loadView('pfd', compact('profile'));


        Mail::to('zanele.maluleke@cullinan.co.za',['cons_email'=>$input])->send(new ProfileFormMail($input));
        Session::flash('success', 'Profile has been updated');
            return redirect()->route('profile.edit',['profile'=>$profile])->with('success','successfully updated profile');
        //return redirect()->back()->with('success','successfully updated profile');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function validateInput($request)
    {
        $this->validate($request, [
            'title' => 'required|max:60',
            'surname' => 'required|regex:/^[\pL\s\-]+$/u|max:60',
            'first_names' => 'required|max:60|regex:/^[\pL\s\-]+$/u',
            'gender' => 'required',
            'date_of_birth' => 'required',
            'nationality' => 'required',
            'passenger_email' => 'required|email|max:80',
            'street_address' => 'required',
            'suburb' => 'required',
            'city' => 'required',
            'province' => 'required',
            'passenger_mobile_num' => 'required|regex:/(0)[0-9]{9}/',
            'passport_number' => 'required|max:15',
            'emergency_contact_number' => 'required|regex:/(0)[0-9]{9}/',
            'emergency_contact_relation' => 'required',
            'emergency_relationship' => '',
            'id_number' => 'required',
            //'crud_url_unique' => '',
            'special_requests' => '',


        ]);


    }

    private function createQueryInput($keys, $request)
    {
        $queryInput = [];
        for ($i = 0; $i < sizeof($keys); $i++) {
            $key = $keys[$i];
            $queryInput[$key] = $request[$key];
        }

        return $queryInput;
    }
}
