<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crud', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('users_id')->references('id')->on('users');
            $table->string('title');
            $table->string('Surname');
            $table->string('first_names');
            $table->string('gender');
            $table->date('date_of_birth');
            $table->string('nationality');
            $table->string('passport_number');
            $table->string('street_address');
            $table->string('suburb');
            $table->string('city');
            $table->string('province');
            $table->string('passenger_mobile_num');
            $table->string('passenger_email')->unique();
            $table->string('special_requests');
            $table->string('emergency_contact_number');
            $table->string('emergency_contact_relation');
            $table->string('emergency_contact_fullname');
            $table->string('emergency_contact_email');
            $table->string('emergency_relationship');
            $table->string('id_number');
            $table->unique('cons_ref');
            $table->unique('cons_email');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crud');
    }
}
